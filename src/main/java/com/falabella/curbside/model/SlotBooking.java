package com.falabella.curbside.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "slot_booking")
public class SlotBooking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long slotBookingId;

    @Column(name = "store_id")
    private String storeId;

    @Column(name = "slot_date")
    private LocalDate slotDate;

    @Column(name = "slot_start_time")

    private LocalDateTime slotStartTime;

    @Column(name = "slot_end_time")
    private LocalDateTime slotEndTime;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "ruth")
    private String ruth;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "name")
    private String name;
}