package com.falabella.curbside.client;

import com.falabella.curbside.dto.StoreDetailsDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "storeServiceFeignClient", url = "http://localhost:8020/v1")
public interface StoreServiceFeignClient {
    @GetMapping("/store/{storeId}")
    StoreDetailsDTO getStoreDetailsById(@PathVariable String storeId);

}
