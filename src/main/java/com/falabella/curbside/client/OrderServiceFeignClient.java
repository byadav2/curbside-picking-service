package com.falabella.curbside.client;

import com.falabella.curbside.dto.OrderDetailsDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "orderServiceFeignClient", url = "http://localhost:8010/")
public interface OrderServiceFeignClient {
    @GetMapping("/orders/search/findByAccountId")
    OrderDetailsDTO getOrdersByCustomerId(@RequestParam("accountId") String customerId);
}
