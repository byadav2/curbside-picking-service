package com.falabella.curbside.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.falabella.curbside.client.StoreServiceFeignClient;
import com.falabella.curbside.dto.OrderSlotDetails;
import com.falabella.curbside.dto.SlotBookRequestDTO;
import com.falabella.curbside.dto.SlotDetailListResponseDTO;
import com.falabella.curbside.dto.SlotDetailsDTO;
import com.falabella.curbside.dto.SlotDetailsResponseAllStoresDTO.SlotDetailsResponseWrapper;
import com.falabella.curbside.dto.SlotDetailsResponseDTO;
import com.falabella.curbside.dto.StoreDetailsDTO;
import com.falabella.curbside.dto.StoreTimingDetailsDTO;
import com.falabella.curbside.exception.BookingException;
import com.falabella.curbside.model.SlotBooking;
import com.falabella.curbside.repository.SlotBookingRepository;
import com.falabella.curbside.service.SlotBookingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
public class SlotBookingServiceImpl implements SlotBookingService {
    @Autowired
    SlotBookingRepository slotBookingRepository;
    @Autowired
    StoreServiceFeignClient storeServiceFeignClient;

    @Override
    public List<SlotDetailListResponseDTO> getSlots(String storeId, double distance, LocalDate slotDate) {
        StoreDetailsDTO storeDetailsDTO = getStoreDetails(storeId);
        Long slotDurationInMillis = storeDetailsDTO.getCollectTimeInMillisec();
        Integer allowedCustomersPerSlot = storeDetailsDTO.getCustomerPerSlot();
        return storeDetailsDTO.getStoreTimingDetails()
                .stream()
                .filter(storeTimingDetailsDTO -> slotDate == null || storeTimingDetailsDTO.getDate().isEqual(slotDate))
                .map(storeTimingDetailsDTO -> generatePossibleSlots(storeTimingDetailsDTO, slotDurationInMillis))
                .map(slotDetailsResponseDTO -> removeUnavailableSlots(storeId, slotDetailsResponseDTO, allowedCustomersPerSlot))
                .map((SlotDetailsResponseDTO slotDetailsResponseDTO1) -> mapToSlotDetailListResponse(slotDetailsResponseDTO1, distance))
                .collect(Collectors.toList());
    }

    private SlotDetailListResponseDTO mapToSlotDetailListResponse(SlotDetailsResponseDTO slotDetailsResponseDTO, double distance) {
        SlotDetailListResponseDTO slotDetailListResponseDTO = new SlotDetailListResponseDTO();
        slotDetailListResponseDTO.setDistance(distance);
        slotDetailListResponseDTO.setSlotDate(slotDetailsResponseDTO.getSlotDate());
        slotDetailListResponseDTO.setSlotDetailsDTOList(slotDetailsResponseDTO.getSlotDetailsDTOMap().values());
        return slotDetailListResponseDTO;
    }

    private SlotDetailsResponseDTO removeUnavailableSlots(String storeId, SlotDetailsResponseDTO slotDetailsResponseDTO, Integer allowedCustomersPerSlot) {
        List<SlotBooking> slotBookings = slotBookingRepository.findAllByStoreIdAndSlotDate(storeId, slotDetailsResponseDTO.getSlotDate());
        Map<ImmutablePair<LocalDateTime, LocalDateTime>, List<SlotBooking>> groupedSlotBookings =
                slotBookings.stream()
                        .collect(Collectors.groupingBy(slotBooking -> new ImmutablePair<>(slotBooking.getSlotStartTime(), slotBooking.getSlotEndTime())));
        List<String> unavailableStartEndTimeList = new ArrayList<>();
        for (Map.Entry<ImmutablePair<LocalDateTime, LocalDateTime>, List<SlotBooking>> startEndTimePairEntry : groupedSlotBookings.entrySet()) {
            if (startEndTimePairEntry.getValue().size() == allowedCustomersPerSlot) {
                unavailableStartEndTimeList.add(startEndTimePairEntry.getKey().getLeft() + "-" + startEndTimePairEntry.getKey().getRight());
            }
        }
        for (String unavailableStartEndTime : unavailableStartEndTimeList) {
            slotDetailsResponseDTO.getSlotDetailsDTOMap().remove(unavailableStartEndTime);
        }
        return slotDetailsResponseDTO;
    }

    private SlotDetailsResponseDTO generatePossibleSlots(StoreTimingDetailsDTO storeTimingDetailsDTO,
                                                             Long slotDurationInMillis) {
        Map<String, SlotDetailsDTO> slotDetailsDTOMap = new TreeMap<>();
        LocalDateTime startTime = storeTimingDetailsDTO.getDayStartTime();
        while (startTime.isBefore(storeTimingDetailsDTO.getDayEndTime())) {
            LocalDateTime startSlotTime = startTime;
            LocalDateTime endSlotTime = startTime.plus(slotDurationInMillis, ChronoUnit.MILLIS);
            slotDetailsDTOMap.put(startSlotTime + "-" + endSlotTime, new SlotDetailsDTO(startSlotTime, endSlotTime));
            startTime = endSlotTime;
        }
        return new SlotDetailsResponseDTO(10, storeTimingDetailsDTO.getDate(), slotDetailsDTOMap);
    }

    @Override
    @Transactional
    public void bookSlot(SlotBookRequestDTO bookRequestDTO) {
        StoreDetailsDTO storeDetailsDTO = getStoreDetails(bookRequestDTO.getStoreId());
        if (isSlotAvailable(storeDetailsDTO.getCustomerPerSlot(), bookRequestDTO)) {
            SlotBooking slotBooking = getSlotBooking(bookRequestDTO);
            slotBookingRepository.save(slotBooking);
        } else {
            throw new BookingException("Slots not available for the request date and time");
        }
    }

    @Override
    public List<SlotDetailsResponseWrapper> getNearbyStoreSlots(LocalDate slotDate) {
        try {
            List<SlotDetailsResponseWrapper> slotDetailsResponseWrapperList = new ArrayList<>();

            CompletableFuture<List<SlotDetailListResponseDTO>> completableFuture1 =
                    CompletableFuture.supplyAsync(() -> getSlots("110", 6.8, slotDate));
            CompletableFuture<List<SlotDetailListResponseDTO>> completableFuture2 =
                    CompletableFuture.supplyAsync(() -> getSlots("111", 7.2, slotDate));
            List<SlotDetailListResponseDTO> slotDetailListResponseDTOS2 = completableFuture2.get();
            List<SlotDetailListResponseDTO> slotDetailListResponseDTOS1 = completableFuture1.get();

            slotDetailsResponseWrapperList.add(new SlotDetailsResponseWrapper("110", slotDetailListResponseDTOS1));
            slotDetailsResponseWrapperList.add(new SlotDetailsResponseWrapper("111", slotDetailListResponseDTOS2));
            return slotDetailsResponseWrapperList;

        } catch (Exception e) {
            log.error("No nearby stores found");
        }
        return null;
    }

    @Override
    public List<OrderSlotDetails> getOrders(String[] orderIds) {
        List<SlotBooking> slotBookingList = slotBookingRepository.findAllByOrderIdIn(Arrays.asList(orderIds));
        if (!CollectionUtils.isEmpty(slotBookingList)) {
            String storeId = slotBookingList.get(0).getStoreId();
            StoreDetailsDTO storeDetailsDTO = getStoreDetails(storeId);
            return slotBookingList.stream().map(slotBooking -> mapToOrderSlotDetails(slotBooking, storeDetailsDTO)).collect(Collectors.toList());
        }
        return null;
    }

    private OrderSlotDetails mapToOrderSlotDetails(SlotBooking slotBooking, StoreDetailsDTO storeDetailsDTO) {
        OrderSlotDetails.BookingDetails bookingDetails = new OrderSlotDetails.BookingDetails();
        bookingDetails.setSlotDate(slotBooking.getSlotDate());
        bookingDetails.setName(slotBooking.getName());
        bookingDetails.setRuth(slotBooking.getRuth());
        bookingDetails.setSlotStartTime(slotBooking.getSlotStartTime());
        bookingDetails.setSlotEndTime(slotBooking.getSlotEndTime());
        bookingDetails.setStoreId(slotBooking.getStoreId());
        bookingDetails.setLatitude(storeDetailsDTO.getLatitude());
        bookingDetails.setLongitude(storeDetailsDTO.getLongitude());
        return new OrderSlotDetails(slotBooking.getOrderId(), bookingDetails);
    }


    private SlotBooking getSlotBooking(SlotBookRequestDTO bookRequestDTO) {
        SlotBooking slotBooking = new SlotBooking();
        slotBooking.setStoreId(bookRequestDTO.getStoreId());
        slotBooking.setSlotDate(bookRequestDTO.getSlotDate());
        slotBooking.setSlotStartTime(bookRequestDTO.getSlotStartTime());
        slotBooking.setSlotEndTime(bookRequestDTO.getSlotEndTime());
        slotBooking.setCustomerId(bookRequestDTO.getCustomerId());
        slotBooking.setOrderId(bookRequestDTO.getOrderId());
        slotBooking.setRuth(bookRequestDTO.getRuth());
        slotBooking.setName(bookRequestDTO.getName());
        return slotBooking;
    }

    private StoreDetailsDTO getStoreDetails(String storeId) {
        return storeServiceFeignClient.getStoreDetailsById(storeId);
    }

    private List<SlotBooking> getSlotBookingDetails(String storeId, LocalDateTime slotStartTime, LocalDateTime slotEndTime) {
        return slotBookingRepository.findByStoreIdAndSlotStartTimeGreaterThanEqualAndSlotEndTimeLessThanEqual(storeId, slotStartTime, slotEndTime);
    }

    private boolean isSlotAvailable(Integer numberOfPersonsPerSlot, SlotBookRequestDTO bookRequestDTO) {
        List<SlotBooking> slotBookingDetails = getSlotBookingDetails(bookRequestDTO.getStoreId(), bookRequestDTO.getSlotStartTime(), bookRequestDTO.getSlotEndTime());
        return CollectionUtils.isEmpty(slotBookingDetails) || slotBookingDetails.size() < numberOfPersonsPerSlot;
    }
}
