package com.falabella.curbside.service;

import java.time.LocalDate;
import java.util.List;

import com.falabella.curbside.dto.OrderSlotDetails;
import com.falabella.curbside.dto.SlotBookRequestDTO;
import com.falabella.curbside.dto.SlotDetailListResponseDTO;
import com.falabella.curbside.dto.SlotDetailsResponseAllStoresDTO.SlotDetailsResponseWrapper;

public interface SlotBookingService {
    List<SlotDetailListResponseDTO> getSlots(String storeId, double distance, LocalDate slotDate);
    void bookSlot(SlotBookRequestDTO bookRequestDTO);

    List<SlotDetailsResponseWrapper> getNearbyStoreSlots(LocalDate slotDate);

    List<OrderSlotDetails> getOrders(String[] orderIds);
}
