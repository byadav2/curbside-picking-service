package com.falabella.curbside.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.falabella.curbside.model.SlotBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SlotBookingRepository extends JpaRepository<SlotBooking, Long> {
    List<SlotBooking> findAllByOrderIdIn(List<String> orderId);
    List<SlotBooking> findAllByStoreIdAndSlotDate(String storeId, LocalDate slotDate);
    List<SlotBooking> findByStoreIdAndSlotStartTimeGreaterThanEqualAndSlotEndTimeLessThanEqual(String storeId, LocalDateTime slotStartTime, LocalDateTime slotEndTime);
}
