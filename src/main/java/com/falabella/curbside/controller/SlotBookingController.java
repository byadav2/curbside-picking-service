package com.falabella.curbside.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.falabella.curbside.dto.OrderSlotDetails;
import com.falabella.curbside.dto.SlotBookRequestDTO;
import com.falabella.curbside.dto.SlotDetailsResponseAllStoresDTO;
import com.falabella.curbside.dto.SlotDetailsResponseAllStoresDTO.SlotDetailsResponseWrapper;
import com.falabella.curbside.service.SlotBookingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
@Slf4j
public class SlotBookingController {

    @Autowired
    SlotBookingService slotBookingService;

    @GetMapping("/slots")
    public ResponseEntity<SlotDetailsResponseAllStoresDTO> getSlots(@RequestParam("store_id") String storeId,
                                                                 @RequestParam(value = "slot_date", required = false) String slotDate,
                                                                 @RequestParam(value = "lat", required = false) String lat,
                                                                 @RequestParam(value = "lng", required = false) String lng) {
        LocalDate slotDateLocal = null;
        if (StringUtils.isNotBlank(slotDate)) {
            slotDateLocal = LocalDate.parse(slotDate);
        }

        SlotDetailsResponseWrapper slotDetailsResponseWrapper = new SlotDetailsResponseWrapper();
        slotDetailsResponseWrapper.setStoreId(storeId);
        slotDetailsResponseWrapper.setAllDaysSlotDetailList(slotBookingService.getSlots(storeId, 5.25, slotDateLocal));


        List<SlotDetailsResponseWrapper> slotDetailsResponseWrapperList = slotBookingService.getNearbyStoreSlots(slotDateLocal);

        List<SlotDetailsResponseWrapper> allStoreWrapperList = new ArrayList<>();
        allStoreWrapperList.add(slotDetailsResponseWrapper);
        if (!CollectionUtils.isEmpty(slotDetailsResponseWrapperList)) {
            allStoreWrapperList.addAll(slotDetailsResponseWrapperList);
        }
        SlotDetailsResponseAllStoresDTO slotDetailsResponseAllStoresDTO = new SlotDetailsResponseAllStoresDTO(allStoreWrapperList);
        return ResponseEntity.ok(slotDetailsResponseAllStoresDTO);
    }

    @PostMapping("/slots")
    public void bookSlots(@RequestBody SlotBookRequestDTO bookRequestDTO) {
        slotBookingService.bookSlot(bookRequestDTO);
    }

    @GetMapping("/orders")
    public ResponseEntity<List<OrderSlotDetails>> getOrders(@RequestParam("orderIds") String[] orderIds) {
        return ResponseEntity.ok(slotBookingService.getOrders(orderIds));
    }
}
