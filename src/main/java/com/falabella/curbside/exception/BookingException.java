package com.falabella.curbside.exception;

public class BookingException extends RuntimeException {

    public BookingException(String message) {
        super(message);
    }
}
