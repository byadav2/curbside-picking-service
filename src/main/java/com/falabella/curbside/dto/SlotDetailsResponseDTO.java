package com.falabella.curbside.dto;

import java.time.LocalDate;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlotDetailsResponseDTO {
    int distance;
    LocalDate slotDate;
    Map<String, SlotDetailsDTO> slotDetailsDTOMap;
}
