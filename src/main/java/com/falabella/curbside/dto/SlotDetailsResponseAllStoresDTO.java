package com.falabella.curbside.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlotDetailsResponseAllStoresDTO {
    List<SlotDetailsResponseWrapper> storeDetails;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SlotDetailsResponseWrapper {
        String storeId;
        List<SlotDetailListResponseDTO> allDaysSlotDetailList;
    }
}
