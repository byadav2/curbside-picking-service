package com.falabella.curbside.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SlotDetailsDTO {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime slotStartTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime slotEndTime;
}
