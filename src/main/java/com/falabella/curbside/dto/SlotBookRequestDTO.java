package com.falabella.curbside.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SlotBookRequestDTO {
    String storeId;
    LocalDate slotDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime slotStartTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime slotEndTime;
    String customerId;
    String orderId;
    String ruth;
    String name;
}
