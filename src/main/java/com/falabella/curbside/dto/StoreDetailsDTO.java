package com.falabella.curbside.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreDetailsDTO {
    String id;
    Integer customerPerSlot;
    Long collectTimeInMillisec;
    Double latitude;
    Double longitude;
    List<StoreTimingDetailsDTO> storeTimingDetails;
}
