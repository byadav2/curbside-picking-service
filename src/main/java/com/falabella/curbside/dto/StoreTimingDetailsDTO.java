package com.falabella.curbside.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreTimingDetailsDTO {
    LocalDate date;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime dayStartTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime dayEndTime;
}
