CREATE TABLE slot_booking (
    slot_booking_id bigint PRIMARY KEY auto_increment,
    store_id VARCHAR,
    slot_date DATE,
    slot_start_time TIMESTAMP,
    slot_end_time TIMESTAMP,
    customer_id VARCHAR,
    name VARCHAR,
    ruth VARCHAR,
    order_id VARCHAR
);
